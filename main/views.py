from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    return render(request=request,
                  template_name='index.html', context={'user': request.user})


def about(request):
    return render(request=request,
                  template_name='about.html', context={'user': request.user})
