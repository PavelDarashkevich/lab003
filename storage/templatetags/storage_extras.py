import datetime
import os

from django import template
from django.urls import reverse_lazy

from storage.core.engine import StorageFileInfo, TrashFileInfo, get_trash_engine
from storage.core.utils import PathThroughoutUrl

register = template.Library()


@register.filter
def path_encode(path):
    return PathThroughoutUrl.encode(path)


@register.filter
def path_decode(path):
    return PathThroughoutUrl.decode(path)


@register.filter
def path_dirname(path):
    return os.path.dirname(path)


@register.filter(name='path_join')
def path_join(path, *paths):
    return os.path.join(path, *paths)


@register.filter
def time_to_date(time):
    return datetime.datetime.fromtimestamp(time)


@register.simple_tag
def url_view_storages():
    return reverse_lazy('storages:view_storages', args=[])


@register.simple_tag
def url_view_storage(storage, path=None):
    if path is None:
        return reverse_lazy('storages:view_storage', args=[storage.id])
    return reverse_lazy('storages:view_storage', args=[storage.id, path_encode(path)])


@register.simple_tag
def url_view_trashes():
    return reverse_lazy('storages:view_trashes')


@register.simple_tag
def url_view_trash(trash, path=None):
    if path is None:
        return reverse_lazy('storages:view_trash', args=[trash.id])
    return reverse_lazy('storages:view_trash', args=[trash.id, path_encode(path)])


@register.simple_tag
def get_storage_dir_tree(storage, path):
    ret = []
    while path != u'' and path != u'/':
        ret.append(StorageFileInfo(storage, path))
        head, tail = os.path.split(path)

        path = head

    ret.reverse()
    return ret


@register.simple_tag
def get_trash_dir_tree(trash_model, path):
    trash_engine = get_trash_engine(trash_model)

    ret = []
    while path != u'' and path != u'/':
        ret.append(TrashFileInfo(trash_model, trash_engine, path))
        head, tail = os.path.split(path)

        path = head

    ret.reverse()
    return ret


@register.simple_tag
def url_task_move_to_trash():
    return reverse_lazy('storages:task_move_to_trash')


@register.simple_tag
def url_task_remove():
    return reverse_lazy('storages:task_remove_from_trash')


@register.simple_tag
def url_task_restore():
    return reverse_lazy('storages:task_restore')


@register.simple_tag
def url_view_tasks():
    return reverse_lazy('storages:view_tasks')


@register.simple_tag
def url_view_trash_tasks(trash):
    return reverse_lazy('storages:view_trash_tasks', args=[trash.id])


@register.simple_tag
def url_add_trash():
    return reverse_lazy('storages:add_trash')


@register.simple_tag
def url_add_storage():
    return reverse_lazy('storages:add_storage')


@register.simple_tag
def url_remove_trash(trash):
    return reverse_lazy('storages:remove_trash', args=[trash.id])


@register.simple_tag
def url_remove_storage(storage):
    return reverse_lazy('storages:remove_storage', args=[storage.id])


@register.simple_tag
def url_trash_settings(trash):
    return reverse_lazy('storages:trash_settings', args=[trash.id])


@register.simple_tag
def url_storage_settings(storage):
    return reverse_lazy('storages:storage_settings', args=[storage.id])


@register.simple_tag
def url_storage_save_settings(storage):
    return reverse_lazy('storages:storage_settings', args=[storage.id])


@register.simple_tag
def url_trash_save_settings(trash):
    return reverse_lazy('storages:trash_settings', args=[trash.id])


@register.simple_tag
def url_view_task(task):
    return reverse_lazy('storages:task_view', args=[task['app_id']])
