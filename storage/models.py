import datetime
from django.contrib.auth.models import User
from django.db import models


class Storage(models.Model):
    name = models.CharField(max_length=100, null=False)
    path = models.CharField(max_length=100, unique=True, null=False)


class Trash(models.Model):
    name = models.CharField(max_length=100, null=False)
    path = models.CharField(max_length=100, null=False)


class UserStorages(models.Model):
    user = models.ForeignKey(User, null=False)
    storage = models.ForeignKey(Storage, null=False)

    class Meta:
        unique_together = ('user', 'storage',)


class UserTrashes(models.Model):
    user = models.ForeignKey(User, null=False)
    trash = models.ForeignKey(Trash, null=False)

    class Meta:
        unique_together = ('user', 'trash',)


class Task(models.Model):
    app_id = models.CharField(unique=True, max_length=100)
    trash = models.ForeignKey(Trash, null=False)
    type = models.CharField(max_length=100)
    src = models.CharField(max_length=1000)
    dst = models.CharField(max_length=1000)
    datetime_start = models.DateTimeField(default=datetime.datetime.now, blank=True)
