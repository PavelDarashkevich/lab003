from django.conf.urls import url

from storage.views import tasks, storages, trashes

_storage_pattern = r'(?P<storage_id>[0-9]+)'
_trash_pattern = r'(?P<trash_id>[0-9]+)'
_relpath_pattern = r'(?P<encrypted_relative_path>[A-Za-z0-9=\-_]*)'
_task_pattern = r'(?P<task_id>[A-Za-z0-9-]+)'

urlpatterns = [
    url(r'^storages/$', storages.StoragesView.as_view(), name='view_storages'),
    url(r'^storages/add/$', storages.AddStorageView.as_view(), name='add_storage'),

    url(r'^storage/{storage}/view/{relpath}?$'.format(
        storage=_storage_pattern, relpath=_relpath_pattern),
        storages.StorageView.as_view(), name='view_storage'),
    url(r'^storage/{storage}/remove/$'.format(storage=_storage_pattern),
        storages.RemoveStorageView.as_view(), name='remove_storage'),
    url(r'^storage/{storage}/settings/$'.format(storage=_storage_pattern),
        storages.StorageSettingsView.as_view(), name='storage_settings'),

    url(r'^tasks/view/$', tasks.TasksView.as_view(), name='view_tasks'),
    url(r'^tasks/new/move_to_trash/$',
        tasks.TaskMoveToTrashView.as_view(), name='task_move_to_trash'),
    url(r'^tasks/new/restore/$',
        tasks.TaskRestoreView.as_view(), name='task_restore'),
    url(r'^tasks/new/erase/$',
        tasks.TaskEraseView.as_view(), name='task_remove_from_trash'),
    url(r'^task/{task}/view$'.format(task=_task_pattern),
        tasks.TaskView.as_view(), name='task_view'),


    url(r'^trashes/$',
        trashes.TrashesView.as_view(), name='view_trashes'),
    url(r'^trashes/add/$',
        trashes.AddTrashView.as_view(), name='add_trash'),

    url(r'^trash/{trash}/view/{relpath}?$'.format(
        storage=_storage_pattern, trash=_trash_pattern, relpath=_relpath_pattern),
        trashes.TrashView.as_view(), name='view_trash'),

    # url(r'^trash/{trash}/tasks/$'.format(trash=_trash_pattern),
    #     tasks.TasksView.as_view(), name='view_trash_tasks'),
    url(r'^trash/{trash}/remove/$'.format(trash=_trash_pattern),
        trashes.RemoveTrashView.as_view(), name='remove_trash'),
    url(r'^trash/{trash}/settings/$'.format(trash=_trash_pattern),
        trashes.TrashSettingsView.as_view(), name='trash_settings'),
]
