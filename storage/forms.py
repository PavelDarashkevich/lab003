from django import forms
from storage import models


class AddStorageForm(forms.ModelForm):
    class Meta:
        model = models.Storage
        fields = ['name', 'path']


class AddTrashForm(forms.ModelForm):
    class Meta:
        model = models.Trash
        fields = ['name', 'path']


class MoveToTrashForm(forms.Form):
    def __init__(self, *args, **kwargs):
        trashes = kwargs.pop('trashes')

        super(MoveToTrashForm, self).__init__(*args, **kwargs)

        trash_choices = [(trash.id, trash.name) for trash in trashes]
        self.trash = forms.MultipleChoiceField(choices=trash_choices, required=True)

    paths = forms.CheckboxSelectMultiple()
