from __future__ import print_function

import re

from celery import shared_task
from smrm.config.config import Config, OutputMode, ActionMode

from storage import models
from storage.core import engine
from storage.core.file_info import StorageFileInfo


def create_trash_engine(trash_model, dry_run, regex_filter=None, action_mode=None):
    config = Config()
    config.output_mode = OutputMode.NO_OUTPUT
    if action_mode == 'skip':
        config.action_mode = ActionMode.SKIP
    elif action_mode == 'force':
        config.action_mode = ActionMode.FORCE
    elif action_mode == 'fail' or action_mode is None:
        config.action_mode = ActionMode.FAIL
    else:
        raise ValueError('Invalid prompt_mode')

    trash_engine = engine.get_trash_engine(trash_model, dry_run, config)

    trash_engine.file_system.name_filter = lambda name: (
        len(re.findall(regex_filter or '', name)) != 0)

    return trash_engine


@shared_task
def _move_to_trash(absolute_path, trash_model_id, dry_run, regex_filter=None, action_mode=None):
    trash_model = models.Trash.objects.get(id=trash_model_id)

    trash_engine = create_trash_engine(trash_model, dry_run, regex_filter, action_mode)
    trash_engine.move_object(absolute_path)


def move_to_trash(storage_model, relative_path, trash_model, dry_run, regex_filter=None, action_mode=None):
    file_info = StorageFileInfo(storage_model, relative_path)

    app_task = _move_to_trash.delay(file_info.absolute_path, trash_model.id, dry_run, regex_filter, action_mode)
    task_model = models.Task(
        app_id=app_task.task_id,
        trash=trash_model,
        type='move_to_trash',
        src=file_info.absolute_path,
        dst=trash_model.path,
    )
    task_model.save()

    return app_task.task_id


@shared_task
def _restore_from_trash(trash_model_id, path, dry_run, new_path=None, regex_filter=None, action_mode=None):
    try:
        trash_model = models.Trash.objects.get(id=trash_model_id)

        trash_engine = create_trash_engine(trash_model, dry_run, regex_filter, action_mode)
        trash_engine.restore_file(path, new_path)
    except Exception as e:
        return e


def restore_from_trash(trash_model, path, dry_run, new_path=None, regex_filter=None, action_mode=None):
    app_task = _restore_from_trash.delay(trash_model.id, path, dry_run, new_path, regex_filter, action_mode)
    task_model = models.Task(
        app_id=app_task.task_id,
        trash=trash_model,
        type='restore_from_trash',
        src=path,
        dst=new_path or '',
    )
    task_model.save()

    return app_task.task_id


@shared_task
def _erase_from_trash(trash_model_id, path, dry_run, regex_filter=None, action_mode=None):
    try:
        trash_model = models.Trash.objects.get(id=trash_model_id)

        trash_engine = create_trash_engine(trash_model, dry_run, regex_filter, action_mode)
        trash_engine.erase_object(path)
    except Exception as e:
        return e


def erase_from_trash(trash_model, path, dry_run, regex_filter=None, action_mode=None):
    app_task = _erase_from_trash.delay(trash_model.id, path, dry_run, regex_filter, action_mode)
    task_model = models.Task(
        app_id=app_task.task_id,
        trash=trash_model,
        type='erase_from_trash',
        src=path,
        dst=''
    )
    task_model.save()

    return app_task.task_id

