import errno
import os

from smrm.actions import FileSystemActions
from smrm.config.config import Config, ActionMode, OutputMode
from smrm.file_system import FileSystem
from smrm.trash.trash import Trash
from smrm.user_questions import UserQuestions

from storage import models
from storage.core.file_info import StorageFileInfo, TrashFileInfo
from storage.core.utils import FileUtils


class StorageNotFoundException(Exception):
    pass


class TrashNotFoundException(Exception):
    pass


def create_new_storage(user, storage_name, storage_dirname):
    if storage_dirname is None:
        return False

    new_storage = models.Storage(name=storage_name,
                                 path=storage_dirname)
    new_storage.save()

    user_storage = models.UserStorages(user=user, storage=new_storage)
    user_storage.save()
    return True


def create_new_trash(user, trash_name, trash_path):
    absolute_trash_path = os.path.abspath(trash_path)
    # if not FileUtils.is_path_in_dir(absolute_trash_path, storage.path):
    #     return False

    new_trash = models.Trash(name=trash_name,
                             path=absolute_trash_path)
    new_trash.save()

    user_trashes = models.UserTrashes(user=user,
                                      trash=new_trash)
    user_trashes.save()
    return True


def get_storage_files(storage_model, relative_path=''):
    _init_storage(storage_model)

    current_files_dir = os.path.join(storage_model.path, relative_path)

    if not FileUtils.is_path_in_dir(current_files_dir, storage_model.path):
        return None

    files = []
    for name in os.listdir(current_files_dir):
        path_in_storage = os.path.join(relative_path, name)

        files.append(StorageFileInfo(storage_model, path_in_storage))

    return files


def _init_storage(storage_model):
    if os.path.isdir(storage_model.path):
        return True

    raise OSError(errno.ENOTDIR, os.strerror(errno.ENOTDIR), storage_model.path)


def get_trash_files(trash_model, relative_path=''):
    trash_engine = get_trash_engine(trash_model)

    if relative_path is None or len(relative_path) == 0:
        paths = trash_engine.get_descriptors()
    else:
        names = os.listdir(trash_engine.object_info(relative_path)['absolute_path_in_trash'])
        paths = [os.path.join(relative_path, name) for name in names]

    files = []
    for path in paths:
        files.append(TrashFileInfo(trash_model, trash_engine, path))

    return files


def get_trash_engine(trash_model, dry_run=False, config=None):
    if config is None:
        config = Config()
        config.action_mode = ActionMode.FORCE
        config.output_mode = OutputMode.NO_OUTPUT

    file_system = FileSystem(dry_mode=dry_run)
    user_questions = UserQuestions(config)
    file_system_actions = FileSystemActions(config, file_system, user_questions)

    trash_engine = Trash(trash_path=trash_model.path,
                         file_system=file_system_actions,
                         protected_paths=config.protected_paths)

    trash_engine.repair()

    return trash_engine
