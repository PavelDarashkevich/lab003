import os

from storage.core.utils import FileUtils


class FileInfo(object):
    def __init__(self, absolute_path):
        self.absolute_path = absolute_path

        self.size = os.path.getsize(self.absolute_path)
        self.type = self._get_type(self.absolute_path)

        self.created_time = os.path.getctime(self.absolute_path)
        self.modified_time = os.path.getmtime(self.absolute_path)
        self.accessed_time = os.path.getatime(self.absolute_path)

    def _get_type(self, path):
        if FileUtils.is_directory(path):
            return 'directory'
        if FileUtils.is_file(path):
            return 'file'
        if FileUtils.is_link(path):
            return 'link'
        return 'unknown'


class StorageFileInfo(FileInfo):
    def __init__(self, storage_model, path_in_storage):
        self.storage = storage_model
        self.absolute_path = os.path.abspath(os.path.join(storage_model.path, path_in_storage))
        self.name = os.path.basename(self.absolute_path)

        self.path = path_in_storage
        super(StorageFileInfo, self).__init__(self.absolute_path)


class TrashFileInfo(FileInfo):
    def __init__(self, trash_model, trash_engine, path_in_trash):
        self.trash = trash_model
        self.path = path_in_trash

        object_info = trash_engine.object_info(path_in_trash)
        self.name = object_info['name']
        self.absolute_path = object_info['absolute_path_in_trash']
        self.restore_path = object_info['path_to_restore']

        super(TrashFileInfo, self).__init__(self.absolute_path)
