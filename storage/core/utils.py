import base64
import os

import errno


class PathThroughoutUrl(object):
    @staticmethod
    def encode(string):
        return base64.urlsafe_b64encode(string.encode('UTF-8'))

    @staticmethod
    def decode(data):
        if data is None:
            return None

        missing_padding = len(data) % 4
        if missing_padding != 0:
            data += b'=' * (4 - missing_padding)

        return unicode(base64.urlsafe_b64decode(data.encode('UTF-8')), 'UTF-8')


class FileUtils(object):
    @staticmethod
    def is_directory(path):
        return not os.path.islink(path) and os.path.isdir(path)

    @staticmethod
    def is_file(path):
        return not os.path.islink(path) and os.path.isfile(path)

    @staticmethod
    def is_link(path):
        return os.path.islink(path)

    @staticmethod
    def make_dirs(path, exist_ok=False):
        try:
            os.makedirs(path)
        except OSError as e:
            if exist_ok and e.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    @staticmethod
    def is_path_in_dir(path, base_dir):
        path = os.path.abspath(path)
        base_dir = os.path.abspath(base_dir)

        if not base_dir.endswith('/'):
            base_dir += '/'

        if os.path.isdir(path) and not path.endswith('/'):
            path += '/'

        return path.startswith(base_dir)

