from __future__ import print_function

from django.core.exceptions import PermissionDenied

from storage import models


def get_user_storage(user, storage_id):
    try:
        return models.Storage.objects.get(id=storage_id, userstorages__user=user)
    except models.Storage.DoesNotExist:
        return PermissionDenied('Storage')


def get_user_trash(user, trash_id):
    try:
        return models.Trash.objects.get(id=trash_id, usertrashes__user=user)
    except (models.Trash.DoesNotExist, models.UserTrashes.DoesNotExist):
        return PermissionDenied('Trash')


def get_user_storages(user):
    return models.Storage.objects.filter(userstorages__user=user)


def get_user_trashes(user):
    return models.Trash.objects.filter(usertrashes__user=user)
