# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-06 15:01
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Storage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('path', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='StorageTrashes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('storage', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='storage.Storage')),
            ],
        ),
        migrations.CreateModel(
            name='Trash',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('path', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='UserStorages',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('storage', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='storage.Storage')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='storagetrashes',
            name='trash',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='storage.Trash'),
        ),
        migrations.AlterUniqueTogether(
            name='userstorages',
            unique_together=set([('user', 'storage')]),
        ),
        migrations.AlterUniqueTogether(
            name='storagetrashes',
            unique_together=set([('storage', 'trash')]),
        ),
    ]
