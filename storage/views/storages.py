from __future__ import absolute_import

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import FormView

from storage import models
from storage.core import engine
from storage.core.mixins import get_user_storage, get_user_storages, get_user_trashes
from storage.core.utils import PathThroughoutUrl
from storage.forms import AddStorageForm


class StorageSettingsView(LoginRequiredMixin, FormView):
    form_class = AddStorageForm

    def get(self, request, *args, **kwargs):
        storage = get_user_storage(request.user, kwargs['storage_id'])
        form = self.form_class(instance=storage)

        return render(
            request=request,
            template_name='storage/storage_settings.html',
            context={'storage': storage, 'form': form}
        )

    def post(self, request, *args, **kwargs):
        storage = get_user_storage(request.user, kwargs['storage_id'])
        form = self.form_class(data=request.POST, instance=storage)

        if form.is_valid():
            storage.name = form.cleaned_data['name']
            storage.path = form.cleaned_data['path']
            storage.save()

        return render(
            request=request,
            template_name='storage/storage_settings.html',
            context={'storage': storage, 'form': form}
        )


class RemoveStorageView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        storage = get_user_storage(request.user, kwargs['storage_id'])
        storage.delete()

        return HttpResponseRedirect(reverse_lazy('storages:view_storages'))


class AddStorageView(LoginRequiredMixin, FormView):
    form_class = AddStorageForm
    success_url = reverse_lazy('storages:view_storages')

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, 'storage/add_storage_form.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if not form.is_valid():
            return render(request, 'storage/add_storage_form.html', {'form': form})

        engine.create_new_storage(
            user=request.user,
            storage_name=form.cleaned_data['name'],
            storage_dirname=form.cleaned_data['path']
        )

        return HttpResponseRedirect(self.success_url)


class StorageView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        storage = get_user_storage(request.user, kwargs['storage_id'])

        encrypted_relative_path = kwargs.get('encrypted_relative_path')
        current_relative_path = PathThroughoutUrl.decode(encrypted_relative_path) or ''

        files = None
        error = None
        try:
            files = engine.get_storage_files(storage, current_relative_path)
        except OSError as e:
            error = str(e)

        trashes = get_user_trashes(request.user)

        return render(request=request,
                      template_name='storage/view_storage.html',
                      context={'storage': storage,
                               'files': files,
                               'current_path': current_relative_path,
                               'trashes': trashes,
                               'error': error})


class StoragesView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        storages = get_user_storages(request.user)
        form = AddStorageForm()

        return render(request=request,
                      template_name='storage/view_storages.html',
                      context={'storages': storages, 'form': form})
