from __future__ import absolute_import

from celery.result import AsyncResult
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views import View

from storage import models
from storage.core import tasks
from storage.core.mixins import get_user_trash, get_user_storage
from storage.core.utils import PathThroughoutUrl


def stringize_result(result):
    if result is None:
        return ''

    return repr(result)


def get_task_info(task_model):
    async_result = AsyncResult(task_model.app_id)

    output_task = {
        'app_id': async_result.task_id,
        'status': async_result.status,
        'type': task_model.type,
        'datetime_start': task_model.datetime_start,
        'trash': task_model.trash,
        'result': stringize_result(async_result.result),
        'traceback': str(async_result.traceback)
    }

    return output_task


class TaskEraseView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        paths = [PathThroughoutUrl.decode(path) for path in request.POST.getlist('paths[]')]

        action_mode = request.POST.get('action_mode', 'fail')
        dry_run = True if request.POST.get('dry_run') == 'true' else False

        try:
            trash_id = int(request.POST['trash_id'])
        except ValueError:
            return self.handle_no_permission()

        trash = get_user_trash(request.user, trash_id)

        regex_filter = request.POST.get('regex_filter')
        form_next = request.POST.get('next', '/')

        for path in paths:
            tasks.erase_from_trash(
                trash_model=trash,
                path=path,
                regex_filter=regex_filter,
                action_mode=action_mode,
                dry_run=dry_run)

        success_url = form_next
        return HttpResponseRedirect(success_url)


class TaskRestoreView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        paths = [PathThroughoutUrl.decode(path) for path in request.POST.getlist('paths[]')]
        new_path = PathThroughoutUrl.decode(request.POST.get('new_path', None))

        action_mode = request.POST.get('action_mode', 'fail')
        dry_run = True if request.POST.get('dry_run') == 'true' else False

        try:
            trash_id = int(request.POST['trash_id'])
        except ValueError:
            return self.handle_no_permission()

        trash = get_user_trash(request.user, trash_id)

        regex_filter = request.POST.get('regex_filter')
        form_next = request.POST.get('next', '/')

        for path in paths:
            tasks.restore_from_trash(
                path=path,
                dry_run=dry_run,
                trash_model=trash,
                new_path=new_path,
                regex_filter=regex_filter,
                action_mode=action_mode)

        success_url = form_next
        return HttpResponseRedirect(success_url)


class TaskMoveToTrashView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        paths = [PathThroughoutUrl.decode(path) for path in request.POST.getlist('paths[]')]
        action_mode = request.POST.get('action_mode', 'fail')
        dry_run = True if request.POST.get('dry_run') == 'true' else False

        try:
            trash_id = int(request.POST['trash'])
        except ValueError:
            return self.handle_no_permission()

        trash = get_user_trash(request.user, trash_id)

        regex_filter = request.POST.get('regex_filter')
        form_next = request.POST.get('next', '/')

        storage_id = request.POST.get('storage_id')
        storage = get_user_storage(request.user, storage_id)

        for path in paths:
            tasks.move_to_trash(
                storage_model=storage,
                dry_run=dry_run,
                relative_path=path,
                trash_model=trash,
                regex_filter=regex_filter,
                action_mode=action_mode)

        success_url = form_next
        return HttpResponseRedirect(success_url)


class TasksView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        tasks_model = models.Task.objects.all().order_by('-datetime_start')

        tasks = []
        for task_model in tasks_model:
            output_task = get_task_info(task_model)
            tasks.append(output_task)

        return render(request=request,
                      template_name='storage/view_tasks.html',
                      context={'tasks': tasks})


class TaskView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        task_model = models.Task.objects.get(app_id=kwargs['task_id'])

        task_info = get_task_info(task_model)

        return render(
            request=request,
            template_name='storage/view_task.html',
            context={'task_info': task_info}
        )
