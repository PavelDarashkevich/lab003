from __future__ import absolute_import

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import FormView

from storage.core import engine
from storage.core.mixins import get_user_trash, get_user_trashes
from storage.core.utils import PathThroughoutUrl
from storage.forms import AddTrashForm


class TrashSettingsView(LoginRequiredMixin, FormView):
    form_class = AddTrashForm

    def get(self, request, *args, **kwargs):
        trash = get_user_trash(request.user, kwargs['trash_id'])
        form = self.form_class(instance=trash)

        return render(
            request=request,
            template_name='storage/trash_settings.html',
            context={'trash': trash, 'form': form}
        )

    def post(self, request, *args, **kwargs):
        trash = get_user_trash(request.user, kwargs['trash_id'])
        form = self.form_class(data=request.POST, instance=trash)

        if form.is_valid():
            trash.name = form.cleaned_data['name']
            trash.path = form.cleaned_data['path']
            trash.save()

        return render(
            request=request,
            template_name='storage/trash_settings.html',
            context={'trash': trash, 'form': form}
        )


class RemoveTrashView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        trash = get_user_trash(request.user, kwargs['trash_id'])

        if engine.get_trash_engine(trash).is_empty():
            trash.delete()

            success_url = reverse_lazy('storages:view_trashes')
            return HttpResponseRedirect(success_url)

        return render(
            request=request,
            template_name='storage/remove_trash_failure.html',
            context={'trash': trash, 'reason': 'Trash not empty!'}
        )


class TrashView(LoginRequiredMixin, FormView):
    def get(self, request, *args, **kwargs):
        trash = get_user_trash(request.user, kwargs['trash_id'])

        encrypted_relative_path = kwargs.get('encrypted_relative_path')
        current_relative_path = PathThroughoutUrl.decode(encrypted_relative_path) or ''

        trash_files = None
        error = None
        try:
            trash_files = engine.get_trash_files(trash, current_relative_path)
        except Exception as e:
            error = e

        return render(request=request,
                      template_name='storage/view_trash.html',
                      context={'current_path': current_relative_path,
                               'files': trash_files,
                               'trash': trash,
                               'error': error})


class TrashesView(LoginRequiredMixin, FormView):
    def get(self, request, *args, **kwargs):
        trashes = get_user_trashes(request.user)

        add_trash_form = AddTrashForm(initial=self.initial)

        return render(request=request,
                      template_name='storage/view_trashes.html',
                      context={'trashes': trashes,
                               'add_trash_form': add_trash_form})


class AddTrashView(LoginRequiredMixin, FormView):
    form_class = AddTrashForm

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(
            request=request,
            template_name='storage/add_trash_form.html',
            context={'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if not form.is_valid():
            return render(
                request=request,
                template_name='storage/add_trash_form.html',
                context={'form': form})

        engine.create_new_trash(request.user,
                                form.cleaned_data['name'],
                                form.cleaned_data['path'])

        success_url = reverse_lazy('storages:view_trashes', args=[])
        return HttpResponseRedirect(success_url)
